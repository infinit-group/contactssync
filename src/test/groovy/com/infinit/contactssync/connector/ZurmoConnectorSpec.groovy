package com.infinit.contactssync.connector

import com.infinit.contactssync.config.TestConfig
import com.infinit.contactssync.domain.Contact
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification

import javax.sql.DataSource

/**
 * Created by owahlen on 31.01.14.
 */
@ContextConfiguration(classes = TestConfig, loader = AnnotationConfigContextLoader)
class ZurmoConnectorSpec extends Specification {

    @Autowired
    private DataSource dataSource

    def "loadContacts"() {
        setup:
        ZurmoConnector zurmoConnector = new ZurmoConnector()
        zurmoConnector.dataSource = dataSource

        when:
        List<Contact> contacts = zurmoConnector.loadContacts()

        then:
        contacts.size() == 1
        Contact contact = contacts.first()
        contact.zurmoId == 1
        contact.firstName == 'testFirstName'
        contact.lastName == 'testLastName'
        contact.jobTitle == 'testJobTitle'
        contact.mobilePhone == '0163666666'
        contact.officePhone == '040123456'
        contact.emailAddress == 'test@testCompany.com'
        contact.streetNr == 'testStreet 12'
        contact.postalCode == '12345'
        contact.city == 'testCity'
        contact.country == 'testCountry'
        contact.company == 'testCompany'
        contact.website == 'http://www.testWebsite.com'
    }

}
