package com.infinit.contactssync.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType

import javax.sql.DataSource

/**
 * Created by owahlen on 31.01.14.
 */
@Configuration
@ComponentScan('com.infinit.contactssync')
class TestConfig extends ApplicationConfig {

    @Bean(destroyMethod = 'shutdown')
    DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder.setType(EmbeddedDatabaseType.H2).addDefaultScripts().build();
    }

}
