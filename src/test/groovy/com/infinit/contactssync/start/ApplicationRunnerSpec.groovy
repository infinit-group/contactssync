package com.infinit.contactssync.start

import com.infinit.contactssync.connector.GoogleConnector
import com.infinit.contactssync.connector.ZurmoConnector
import com.infinit.contactssync.config.TestConfig
import com.infinit.contactssync.domain.Contact
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification

/**
 * Created by owahlen on 30.01.14.
 */
@ContextConfiguration(classes = TestConfig, loader = AnnotationConfigContextLoader)
class ApplicationRunnerSpec extends Specification {

    def "fail when run without arguments"() {
        setup:
        ApplicationRunner runner = new ApplicationRunner() {
            protected void fail(String message) {
                throw new RuntimeException(message)
            }
        }

        when:
        runner.run([] as String[])

        then:
        Exception e = thrown(RuntimeException)
        e.message == 'A connection properties file must be given as argument'
    }

    def "run with properties"() {
        setup:
        ApplicationRunner runner = new ApplicationRunner()
        runner.zurmoConnector = GroovyMock(ZurmoConnector) {
            1 * loadContacts() >> [new Contact(zurmoId: 4711)]
        }
        runner.googleConnector = GroovyMock(GoogleConnector)
        runner.zurmoPropertiesAdapter = GroovyMock(ZurmoPropertiesAdapter) {
            getZurmoDbJdcbUrl() >> 'jdbc:mysql://localhost:3305/zurmo'
            getZurmoDbUsername() >> 'zurmoUser'
            getZurmoDbPassword() >> 'zurmoPass'
            getGoogleClientSecretFile() >> 'someone@gmail.com'
            getGoogleApiKey() >> 'BJkwXyCSMQHrZ6usNwugwHlKUGf8vY-438NnvKc'
            getGoogleApplicationName() >> 'contactssync'
        }

        when:
        runner.run(['propertyFileName'] as String[])

        then:
        1 * runner.zurmoPropertiesAdapter.loadProperties('propertyFileName')
        1 * runner.zurmoConnector.setDbJdbcUrl('jdbc:mysql://localhost:3305/zurmo')
        1 * runner.zurmoConnector.setDbUsername('zurmoUser')
        1 * runner.zurmoConnector.setDbPassword('zurmoPass')
        1 * runner.googleConnector.setGoogleUsername('someone@gmail.com')
        1 * runner.googleConnector.setGoogleApiKey('BJkwXyCSMQHrZ6usNwugwHlKUGf8vY-438NnvKc')
        1 * runner.googleConnector.setGoogleApplicationName('contactssync')
        1 * runner.googleConnector.mergeContacts({it.size()==1})
    }

}
