package com.infinit.contactssync.start

import com.infinit.contactssync.config.TestConfig
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.support.AnnotationConfigContextLoader
import spock.lang.Specification

/**
 * Created by owahlen on 31.01.14.
 */
@ContextConfiguration(classes = TestConfig, loader = AnnotationConfigContextLoader)
class PropertyHolderSpec extends Specification {

    def "loadProperties"() {
        setup:
        URL propertiesUrl = this.class.getResource('/contactssync-test.properties')
        File propertiesFile = new File(propertiesUrl.toURI())
        ZurmoPropertiesAdapter propertyHolder = new ZurmoPropertiesAdapter()

        when:
        propertyHolder.loadProperties(propertiesFile.canonicalPath)

        then:
        propertyHolder.zurmoDbJdcbUrl == 'jdbc:mysql://localhost:3305/zurmo'
        propertyHolder.zurmoDbUsername == 'zurmoUser'
        propertyHolder.zurmoDbPassword == 'zurmoPass'
        propertyHolder.googleClientSecretFile == 'someone@gmail.com'
        propertyHolder.googleApiKey == 'BJkwXyCSMQHrZ6usNwugwHlKUGf8vY-438NnvKc'
        propertyHolder.googleApplicationName == 'contactssync'

    }

}
