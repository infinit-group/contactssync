CREATE TABLE person (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  department varchar(64),
  firstname varchar(32),
  jobtitle varchar(64),
  lastname varchar(32),
  mobilephone varchar(24),
  officephone varchar(24),
  officefax varchar(24),
  title_customfield_id int(11) unsigned,
  primaryemail_email_id int(11) unsigned,
  ownedsecurableitem_id int(11) unsigned,
  primaryaddress_address_id int(11) unsigned,
  PRIMARY KEY (id)
);

CREATE TABLE email (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  isinvalid tinyint(1),
  optout tinyint(1),
  emailaddress varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE address (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  city varchar(32),
  country varchar(32),
  invalid tinyint(1),
  postalcode varchar(16),
  street1 varchar(128),
  street2 varchar(128),
  state varchar(32),
  latitude double,
  longitude double,
  PRIMARY KEY (id)
);

CREATE TABLE contact (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  person_id int(11) unsigned,
  industry_customfield_id int(11) unsigned,
  source_customfield_id int(11) unsigned,
  account_id int(11) unsigned,
  companyname varchar(64),
  description text,
  googlewebtrackingid text,
  website varchar(255),
  secondaryaddress_address_id int(11) unsigned,
  secondaryemail_email_id int(11) unsigned,
  state_contactstate_id int(11) unsigned,
  xinglinkcstm varchar(255),
  wichtigkeitcstm_customfield_id int(11) unsigned,
  PRIMARY KEY (id)
);

CREATE TABLE account (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  ownedsecurableitem_id int(11) unsigned,
  industry_customfield_id int(11) unsigned,
  type_customfield_id int(11) unsigned,
  account_id int(11) unsigned,
  description text,
  name varchar(64),
  officephone varchar(24),
  officefax varchar(24),
  annualrevenue double,
  employees int(11) unsigned,
  website varchar(255),
  billingaddress_address_id int(11) unsigned,
  primaryemail_email_id int(11) unsigned,
  secondaryemail_email_id int(11) unsigned,
  shippingaddress_address_id int(11) unsigned,
  aidacstm_customfield_id int(11) unsigned,
  interessecstm_multiplevaluescustomfield_id int(11) unsigned,
  callscstm int(11) unsigned,
  PRIMARY KEY (id)
);
