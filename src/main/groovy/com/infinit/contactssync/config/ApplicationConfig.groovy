package com.infinit.contactssync.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DriverManagerDataSource

import javax.sql.DataSource

/**
 * Created by owahlen on 31.01.14.
 */
@Configuration
@ComponentScan('com.infinit.contactssync')
class ApplicationConfig {

    @Bean
    DataSource dataSource() {
        DataSource dataSource = new DriverManagerDataSource()
        dataSource.setDriverClassName('com.mysql.jdbc.Driver')
        return dataSource
    }

}
