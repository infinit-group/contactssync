package com.infinit.contactssync.domain

/**
 * Created by owahlen on 30.01.14.
 */
class Contact {
    Long zurmoId
    String firstName
    String lastName
    String jobTitle
    String mobilePhone
    String officePhone
    String emailAddress
    String streetNr
    String postalCode
    String city
    String country
    String company
    String website

	void setFirstName(String firstName) {
		this.firstName = firstName?:null
	}

	void setLastName(String lastName) {
		this.lastName = lastName?:null
	}

	void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle?:null
	}

	void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone?:null
	}

	void setOfficePhone(String officePhone) {
		this.officePhone = officePhone?:null
	}

	void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress?:null
	}

	void setStreetNr(String streetNr) {
		this.streetNr = streetNr?:null
	}

	void setPostalCode(String postalCode) {
		this.postalCode = postalCode?:null
	}

	void setCity(String city) {
		this.city = city?:null
	}

	void setCountry(String country) {
		this.country = country?:null
	}

	void setCompany(String company) {
		this.company = company?:null
	}

	void setWebsite(String website) {
		this.website = website?:null
	}

	String toString() {
	    List<String> subStrings = []
	    if(firstName) subStrings << firstName
	    if(lastName) subStrings << lastName
	    subStrings << "($zurmoId)"
        return subStrings.join(' ')
    }
}
