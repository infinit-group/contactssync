package com.infinit.contactssync.connector

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.gdata.client.Query
import com.google.gdata.client.contacts.ContactsService
import com.google.gdata.data.contacts.ContactEntry
import com.google.gdata.data.contacts.ExternalId
import com.google.gdata.data.contacts.ContactFeed
import com.google.gdata.data.extensions.*
import com.google.gdata.util.ServiceException
import com.infinit.contactssync.domain.Contact
import org.springframework.stereotype.Component

/**
 * Created by owahlen on 30.01.14.
 */
@Component
class GoogleConnector {
	private final static String APPLICATION_NAME = 'contactssync'
	private final static String SCOPE = "https://www.google.com/m8/feeds"
	private final static HttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
	private final static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance()

	private FileDataStoreFactory dataStoreFactory
	private File clientSecretFile

	File getDataStoreDir() {
		if (!dataStoreFactory) {
			return null
		}
		return dataStoreFactory.dataDirectory
	}

	void setDataStoreDir(File dataStoreDir) {
		dataStoreFactory = new FileDataStoreFactory(dataStoreDir)
	}

	File getClientSecretFile() {
		return clientSecretFile
	}

	void setClientSecretFile(File clientSecretFile) {
		this.clientSecretFile = clientSecretFile
	}

	void mergeContacts(List<Contact> contacts) {
		ContactsService contactsService = createContactsService()
		List<ContactEntry> contactEntries = getAllContactEntries(contactsService)
		Map<Long, ContactEntry> contactMap = populateContactMapAndDeleteDuplicates(contactEntries)
		Integer i = 1
		for (Contact contact : contacts) {
			ContactEntry googleContact = contactMap[contact.zurmoId]
			if (googleContact) {
				println "Updating contact '${contact}' (${i++}/${contacts.size()})"
				updateGoogleContact(contactsService, googleContact, contact)
			} else {
				println "Creating contact '${contact}' (${i++}/${contacts.size()})"
				createGoogleContact(contactsService, contact)
			}
		}
		deleteOrphanedContacts(contactMap, contacts)
	}

	private void deleteOrphanedContacts(Map<Long, ContactEntry> contactMap, List<Contact> contacts) {
		Map<Long, ContactEntry> orphanedMap = new HashMap<Long, ContactEntry>(contactMap)
		for(Contact contact: contacts) {
			orphanedMap.remove(contact.zurmoId)
		}
		for(ContactEntry contactEntry : orphanedMap.values()) {
			deleteGoogleContact(contactEntry)
		}
	}

	private ContactsService createContactsService() {
		Credential credential = authorize()
		credential.refreshToken()
		ContactsService contactsService = new ContactsService(APPLICATION_NAME)
		contactsService.setOAuth2Credentials(credential)
		return contactsService
	}

	private Credential authorize() throws Exception {

		// load client secrets
		GoogleClientSecrets clientSecrets = null
		clientSecretFile.withReader { Reader reader ->
			clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, reader)
		}

		// validate JSON
		if (!clientSecrets.getDetails().getClientId()) {
			throw new RuntimeException('client_id is missing in client_secret.json file')
		}
		if (!clientSecrets.getDetails().getClientSecret()) {
			throw new RuntimeException('client_secret is missing in client_secret.json file')
		}

		// set up authorization code flow
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, [SCOPE])
				.setDataStoreFactory(dataStoreFactory)
				.build()

		// authorize
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	private List<ContactEntry> getAllContactEntries(ContactsService contactsService) {
		URL feedUrl = new URL("https://www.google.com/m8/feeds/contacts/default/full")
		Query feedQuery = new Query(feedUrl)
		feedQuery.setMaxResults(10000)
		ContactFeed resultFeed = null
		exponentialBackoff {
			resultFeed = contactsService.getFeed(feedQuery, ContactFeed)
		}
		return resultFeed.entries
	}

	private Map<Long, ContactEntry> populateContactMapAndDeleteDuplicates(List<ContactEntry> contactEntries) {
		Map<Long, ContactEntry> contactMap = [:]
		for (ContactEntry contactEntry : contactEntries) {
			ExternalId externalId = contactEntry.externalIds.find { ExternalId externalId ->
				externalId.hasLabel() && externalId.hasValue() && externalId.label == 'zurmoId'
			}
			if (externalId && externalId.value.isLong()) {
				Long zurmoId = Long.valueOf(externalId.value)
				if (contactMap.containsKey(zurmoId)) {
					deleteGoogleContact(contactEntry)
				} else {
					contactMap[zurmoId] = contactEntry
				}
			}
		}
		return contactMap
	}

	private ContactEntry createGoogleContact(ContactsService contactsService, Contact contact) {
		ContactEntry googleContact = new ContactEntry()
		mergeContactIntoGoogleContact(contact, googleContact)
		// Ask the service to insert the new entry
		URL postUrl = new URL('https://www.google.com/m8/feeds/contacts/default/full')
		ContactEntry createdContactEntry = null
		exponentialBackoff {
			createdContactEntry = contactsService.insert(postUrl, googleContact)
		}
		return createdContactEntry

	}

	private ContactEntry updateGoogleContact(ContactsService contactsService, ContactEntry googleContact, Contact contact) {
		Boolean modified = mergeContactIntoGoogleContact(contact, googleContact)
		if (!modified) {
			return googleContact
		}
		// Ask the service to update the entry
		URL editUrl = new URL(googleContact.editLink.href)
		ContactEntry updatedContactEntry = null
		exponentialBackoff {
			updatedContactEntry = contactsService.update(editUrl, googleContact)
		}
		return updatedContactEntry
	}

	private void deleteGoogleContact(ContactEntry contactEntry) {
		exponentialBackoff {
			contactEntry.delete()
		}
	}

	private Boolean mergeContactIntoGoogleContact(Contact contact, ContactEntry googleContact) {
		Boolean modified = false

		// Zurmo Id
		ExternalId externalZurmoId = googleContact.externalIds.find { ExternalId externalId ->
			externalId.label == 'zurmoId'
		}
		if (!externalZurmoId) {
			externalZurmoId = new ExternalId()
			externalZurmoId.setLabel("zurmoId")
			externalZurmoId.setValue(contact.zurmoId.toString())
			externalZurmoId.setImmutable(true)
			googleContact.addExternalId(externalZurmoId)
			modified = true
		}

		// Name
		Name name = googleContact.name
		if (name?.givenName?.value != contact.firstName || name?.familyName?.value != contact.lastName) {
			if (name && !(contact.firstName || contact.lastName)) {
				googleContact.name = null
			} else if (!name && (contact.firstName || contact.lastName)) {
				name = new Name()
				googleContact.name = name
			}
			name.givenName = contact.firstName ? new GivenName(contact.firstName, null) : null
			name.familyName = contact.lastName ? new FamilyName(contact.lastName, null) : null
			List<String> names = []
			if (contact.firstName) names << contact.firstName
			if (contact.lastName) names << contact.lastName
			String fullName = names.join(' ') ?: null
			name.fullName = fullName ? new FullName(fullName, null) : null
			modified = true
		}

		// Email
		Email primaryMail = googleContact.emailAddresses.find { Email email ->
			email.address == contact.emailAddress
		}
		if (primaryMail?.address != contact.emailAddress ||
				contact.emailAddress && primaryMail?.displayName != googleContact.name?.fullName?.value) {
			if (primaryMail && !contact.emailAddress) {
				googleContact.emailAddresses.remove(primaryMail)
			} else if (!primaryMail && contact.emailAddress) {
				primaryMail = new Email()
				googleContact.addEmailAddress(primaryMail)
			}
			primaryMail.address = contact.emailAddress
			primaryMail.displayName = googleContact.name.fullName.value
			primaryMail.rel = 'http://schemas.google.com/g/2005#work'
			primaryMail.primary = true
			modified = true
		}

		// Mobile Phone
		PhoneNumber mobilePhoneNumber = googleContact.phoneNumbers.find { PhoneNumber phoneNumber ->
			phoneNumber.rel == 'http://schemas.google.com/g/2005#home'
		}
		if (mobilePhoneNumber?.phoneNumber != contact.mobilePhone) {
			if (mobilePhoneNumber && !contact.mobilePhone) {
				googleContact.phoneNumbers.remove(mobilePhoneNumber)
			} else if (!mobilePhoneNumber && contact.mobilePhone) {
				mobilePhoneNumber = new PhoneNumber()
				googleContact.addPhoneNumber(mobilePhoneNumber)
			}
			mobilePhoneNumber.setPhoneNumber(contact.mobilePhone)
			mobilePhoneNumber.setRel('http://schemas.google.com/g/2005#home')
			mobilePhoneNumber.setPrimary(true)
			modified = true
		}

		// Office Phone
		PhoneNumber officePhoneNumber = googleContact.phoneNumbers.find { PhoneNumber phoneNumber ->
			phoneNumber.rel == 'http://schemas.google.com/g/2005#work'
		}
		if (officePhoneNumber?.phoneNumber != contact.officePhone) {
			if (officePhoneNumber && !contact.officePhone) {
				googleContact.phoneNumbers.remove(officePhoneNumber)
			} else if (!officePhoneNumber && contact.officePhone) {
				officePhoneNumber = new PhoneNumber()
				googleContact.addPhoneNumber(officePhoneNumber)
			}
			officePhoneNumber.phoneNumber = contact.officePhone
			officePhoneNumber.rel = 'http://schemas.google.com/g/2005#work'
			officePhoneNumber.primary = !mobilePhoneNumber?.primary
			modified = true
		}

		// Address
		StructuredPostalAddress address = googleContact.structuredPostalAddresses.find { StructuredPostalAddress address ->
			address.rel == 'http://schemas.google.com/g/2005#work'
		}
		if (address?.street?.value != contact.streetNr ||
				address?.city?.value != contact.city ||
				address?.postcode?.value != contact.postalCode ||
				address?.country?.value != contact.country
		) {
			if (address && !(contact.streetNr || contact.city || contact.postalCode || contact.country)) {
				googleContact.structuredPostalAddresses.remove(address)
			} else if (!address && (contact.streetNr || contact.city || contact.postalCode || contact.country)) {
				address = new StructuredPostalAddress()
				googleContact.addStructuredPostalAddress(address)
			}
			address.street = contact.streetNr ? new Street(contact.streetNr) : null
			address.city = contact.city ? new City(contact.city) : null
			address.postcode = contact.postalCode ? new PostCode(contact.postalCode) : null
			address.country = contact.country ? new Country(null, contact.country) : null
			address.formattedAddress = (contact.streetNr || contact.city || contact.postalCode) ? new FormattedAddress("$contact.streetNr $contact.postalCode $contact.city") : null
			address.setRel('http://schemas.google.com/g/2005#work')
			address.setPrimary(true)
			modified = true
		}
		return modified
	}

	private void exponentialBackoff(Closure closure) {
		Exception lastException = null
		for (int i = 0; i <= 5; i++) {
			try {
				closure.call()
				return
			} catch (ServiceException serviceException) {
				lastException = serviceException
				if (serviceException.responseBody.contains('Temporary problem')) {
					Thread.sleep(1000 * (2**i))
				}
			}
		}
		throw new RuntimeException("Unable to call Google service", lastException)
	}
}
