package com.infinit.contactssync.connector

import com.infinit.contactssync.domain.Contact
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.stereotype.Component

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by owahlen on 30.01.14.
 */
@Component
class ZurmoConnector {

    private final static String CONTACT_QUERY = '''
SELECT p.id, p.firstname, p.lastname, p.jobtitle, p.mobilephone, p.officephone,
    e.emailaddress,
    a.street1 streetNr, a.postalcode, a.city, a.country,
    f.name company, f.website
FROM person p
LEFT JOIN email e ON p.primaryemail_email_id=e.id
LEFT JOIN address a ON p.primaryaddress_address_id=a.id
LEFT JOIN contact c ON p.id=c.person_id
LEFT JOIN account f ON c.account_id=f.id
ORDER BY p.id
'''

    private JdbcTemplate jdbcTemplate

    DataSource getDataSource() {
	    if(!jdbcTemplate) {
		    return null
	    }
        return (DataSource) jdbcTemplate.dataSource
    }

    @Autowired
    void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource)
    }

    String getDbJdbcUrl() {
        if(!dataSource) {
            return null
        }
        return ((DriverManagerDataSource) dataSource).url
    }

    void setDbJdbcUrl(String url) {
        ((DriverManagerDataSource) dataSource).url = url
    }

    String getDbUsername() {
        if(!dataSource) {
            return null
        }
        return ((DriverManagerDataSource) dataSource).username
    }

    void setDbUsername(String username) {
        ((DriverManagerDataSource) dataSource).username = username
    }

    String getDbPassword() {
        if(!dataSource) {
            return null
        }
        return ((DriverManagerDataSource) dataSource).password
    }

    void setDbPassword(String password) {
        ((DriverManagerDataSource) dataSource).password = password
    }

    List<Contact> loadContacts() {
        List<Contact> contacts = jdbcTemplate.query(CONTACT_QUERY,
                new RowMapper<Contact>() {
                    public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Contact contact = new Contact()
                        contact.with {
                            zurmoId = rs.getLong('id')
                            firstName = rs.getString('firstname')
                            lastName = rs.getString('lastname')
                            jobTitle = rs.getString('jobtitle')
                            mobilePhone = rs.getString('mobilephone')
                            officePhone = rs.getString('officephone')
                            emailAddress = rs.getString('emailaddress')
                            streetNr = rs.getString('streetnr')
                            postalCode = rs.getString('postalcode')
                            city = rs.getString('city')
                            country = rs.getString('country')
                            company = rs.getString('company')
                            website = rs.getString('website')
                        }
                        return contact
                    }
                })
        return contacts
    }
}
