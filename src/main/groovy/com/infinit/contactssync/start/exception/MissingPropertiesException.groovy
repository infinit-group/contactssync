package com.infinit.contactssync.start.exception

/**
 * Created by owahlen on 31.01.14.
 */
class MissingPropertiesException extends RuntimeException {
    MissingPropertiesException(String message) {
        super(message)
    }

    MissingPropertiesException(String message, Throwable cause) {
        super(message, cause)
    }

    MissingPropertiesException(Throwable cause) {
        super(cause)
    }

    protected MissingPropertiesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace)
    }
}
