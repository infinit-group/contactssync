package com.infinit.contactssync.start

import com.infinit.contactssync.connector.GoogleConnector
import com.infinit.contactssync.connector.ZurmoConnector
import com.infinit.contactssync.domain.Contact
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * Created by owahlen on 31.01.14.
 */
@Component
class ApplicationRunner {

    @Autowired
    ZurmoConnector zurmoConnector

    @Autowired
    GoogleConnector googleConnector

    @Autowired
    ZurmoPropertiesAdapter zurmoPropertiesAdapter

	private File configDir

    void run(String[] args) {
        configDir = getConfigDir(args)
	    createLock()
        loadZurmoProperties()
        List<Contact> contacts = loadContacts()
        mergeContacts(contacts)
    }

    protected void fail(String message) {
        System.err.println "Error: $message"
        System.exit(-1)
    }

    private File getConfigDir(String[] args) {
        if (args.length < 1) {
            fail("The configuration directory must be provided as first argument")
        }
	    File configDirCandidate = new File(args[0])
	    if(!configDirCandidate.exists()) {
		    fail "The directory $configDirCandidate does not exist"
	    }
	    if(!configDirCandidate.isDirectory()) {
		    fail "$configDirCandidate is not a directory"
	    }
	    return configDirCandidate
    }

	private void createLock() {
		File lockFile = new File(configDir, 'lock')
		if(lockFile.exists()) {
			fail "Unable to run since lock file exists: $lockFile.canonicalPath"
		} else {
			lockFile.createNewFile()
			lockFile.deleteOnExit()
		}
	}

    private void loadZurmoProperties() {
        try {
            zurmoPropertiesAdapter.loadProperties(new File(configDir, 'zurmo.properties'))
        } catch (FileNotFoundException e) {
            fail('The properties file could not be found')
        } catch (IOException e) {
            fail('The properties file could not be read')
        }

    }

    private List<Contact> loadContacts() {
	    zurmoConnector.dbJdbcUrl = zurmoPropertiesAdapter.zurmoDbJdcbUrl
	    zurmoConnector.dbUsername = zurmoPropertiesAdapter.zurmoDbUsername
	    zurmoConnector.dbPassword = zurmoPropertiesAdapter.zurmoDbPassword
        List<Contact> contacts = null
        try {
            contacts = zurmoConnector.loadContacts()
        } catch (Exception e) {
            fail("Unable to load contacts from Zurmo ($e.message)")
        }
        return contacts
    }


    private void mergeContacts(List<Contact> contacts) {
	    googleConnector.dataStoreDir = new File(configDir, 'dataStore')
	    googleConnector.clientSecretFile = new File(configDir, 'client_secret.json')
       // try {
            googleConnector.mergeContacts(contacts)
       // } catch (Exception e) {
       //     fail("Unable to merge contacts into google address book ($e.message)")
       // }
    }

}
