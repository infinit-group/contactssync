package com.infinit.contactssync.start

import com.infinit.contactssync.start.exception.MissingPropertiesException
import org.springframework.stereotype.Component

/**
 * Created by owahlen on 31.01.14.
 */
@Component
class ZurmoPropertiesAdapter {

    private final static ZURMO_DB_JDBC_URL = 'zurmo_db_jdbc_url'
    private final static ZURMO_DB_USERNAME = 'zurmo_db_username'
    private final static ZURMO_DB_PASSWORD = 'zurmo_db_password'

    private final static List<String> PROPERTY_KEYS = [
            ZURMO_DB_JDBC_URL,
            ZURMO_DB_USERNAME,
            ZURMO_DB_PASSWORD
    ]

    private Properties props

    void loadProperties(File propertiesFile) throws IOException {
        if (!propertiesFile.exists()) {
            throw new FileNotFoundException('The properties file could not be found')
        }
        props = new Properties()
        propertiesFile.withReader { Reader reader ->
            props.load(reader)

        }
        validateProperties(props)
    }

    String getZurmoDbJdcbUrl() {
        return getPropertyAsString(ZURMO_DB_JDBC_URL)
    }

    String getZurmoDbUsername() {
        return getPropertyAsString(ZURMO_DB_USERNAME)
    }

    String getZurmoDbPassword() {
        return getPropertyAsString(ZURMO_DB_PASSWORD)
    }

    private String getPropertyAsString(String propertyKey) {
        String propertyValue = props[propertyKey] as String
        return propertyValue.trim()
    }

    private validateProperties(Properties properties) {
        List<String> missingKeys = PROPERTY_KEYS - properties.keySet()
        if (missingKeys) {
            throw new MissingPropertiesException("missing keys in properties file are $missingKeys")
        }
    }

}
