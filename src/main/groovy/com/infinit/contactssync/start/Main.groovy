package com.infinit.contactssync.start

import com.infinit.contactssync.config.ApplicationConfig
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext

/**
 * Created by owahlen on 30.01.14.
 */
class Main {

    static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig)
        ApplicationRunner runner = (ApplicationRunner) ctx.getBean(ApplicationRunner)
        runner.run(args)
    }

}
